<?php

namespace App;

class DummyClass
{
    public $foo = 'bar';

    private function dummyFunction($parameter)
    {
        echo $parameter;
    }
}
